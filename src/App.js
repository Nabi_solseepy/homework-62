import React, { Component } from 'react';
import './App.css';
import HeaderMenu from "./component/HeaderMenu/HeaderMenu";
import {BrowserRouter as Router,  Route} from "react-router-dom";
import About from "./component/About/About";
import Contacts from "./component/Contacts/Contacts";
import Home from "./component/Home/Home";

class App extends Component {
  render() {
    return (

     <Router>
         <div>
             <HeaderMenu/>
             <Route exact path="/" component={Home}/>
             <Route path="/about" exact component={About}/>

             <Route path="/contacts" component={Contacts}/>
         </div>

     </Router>


    );
  }
}

export default App;
