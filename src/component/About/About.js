import React from 'react';
import './About.css';

const About = () => {
    return (
        <div className="about">
           <h1>About</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Eius modi nisi officia ut
                voluptates! Cum eos id odio, odit quaerat
                repellat ullam veniam vero voluptatem.
                Aut consectetur deserunt doloribus soluta.</p>
        </div>
    );
};

export default About;