import React from 'react';
import './HeaderMenu.css'
import {NavLink} from "react-router-dom";

const HeaderMenu = () => {
    return (
        <div className="header">
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/about">About us</NavLink></li>
            <li><NavLink to="/contacts">Contacts</NavLink></li>
        </div>
    );
};

export default HeaderMenu;